<?php
namespace AppBundle\Services;

use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;


class Helpers {
    private $jwt_auth;

    public function __construct($jwt_auth) {
        $this->jwt_auth=$jwt_auth;
    }
    
    public function authCheck($hash, $getIdentity = false) {
        $jwt_auth = $this->jwt_auth;
        $auth = false;
        if($hash != null) {
            if($getIdentity == false) {
                $check_token = $jwt_auth->checkToken($hash);
                if($check_token == true) {
                    $auth=true;
                }
            } else {
                $check_token = $jwt_auth->checkToken($hash, true);
                if(is_object($check_token)) {
                    $auth = $check_token;
                }
            }
        } 
        return $auth;
    }

    public function json($data) {
        $normalizer = array(new GetSetMethodNormalizer());
        $encoder = array("json" => new JsonEncoder());

        $serializer = new Serializer($normalizer, $encoder);
        $json = $serializer->serialize($data, 'json');

        $res = new Response();
        $res->setContent($json);
        $res->headers->set("Content-Type", "application/json");

        return $res;
    }

}

<?php
namespace AppBundle\Services;

use Firebase\JWT\JWT;
/**
 * Description of JwtAuth
 *
 * @author jlavie
 */
class JwtAuth {
    private $manager;
    private $key;

    public function __construct($manager) {
        $this->manager = $manager;
        $this->key =  'secret_pass';
    }
    
    public function signup($email, $pass, $getHash = null) {
        $key =$this->key;
        
        $user = $this->manager->getRepository('BackendBundle:User')->findOneBy(
                    array( 
                        "email" => $email,
                        "password" => $pass
                    )
                );
        $signup=false;
        if(is_object($user)) {
            $signup=true;
        }
        
        if($signup) {
            $token = array(
                "sub" => $user->getId(),
                "email"=> $user->getEmail(),
                "firstname"=> $user->getFirstname(),
                "lastname"=> $user->getLastname(),
                "password"=> $user->getPassword(),                
                "image"=> $user->getImage(),
                "iat" => time(),
                "exp" => time() + (7*24*60*60)
            );
            
            $jwt = JWT::encode($token, $key, 'HS256');
            $decode = JWT::decode($jwt, $key, array('HS256'));
            
            if($getHash != null ) {
                return $jwt;
            } else {
                return $decode;
            }
        } else {
            return array("status"=>"error", "data"=>"Login fail, user is not an object");
        }
    }
    
    public function checkToken($token, $getIdentity = false) {
        $key =$this->key;
        $auth = false;
        try {
            $decode = JWT::decode($token, $key, array('HS256'));
        } catch (\UnexpectedValueException $ex) {
            $auth = false;
        } catch (\DomainException $ex) {
            $auth = false;
        } catch (Exception $ex) {
            $auth = false;
        }
        
        if(isset($decode->sub)) {
            $auth=true;
        } else {
            $auth = false;
        }
        
        if($getIdentity == true) {
            return $decode;
        } else {
            return $auth;
        }
    }
}

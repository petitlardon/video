<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\User;
use BackendBundle\Entity\Video;
use BackendBundle\Entity\Comment;
//use AppBundle\Services\Helpers;

/**
 * Description of CommentController
 *
 * @author jlavie
 */
class CommentController extends Controller {
    public function newAction(Request $req) {
        $helpers = $this->get("app.helpers");
        
        $hash = $req->get("authorization", false);
        $authCheck = $helpers->authCheck($hash);
        
        if($authCheck == true) {
            $identity = $helpers->authCheck($hash, true);
            
            $json = $req->get("json", null);
            if($json != null) {
                $params = json_decode($json);
                $createdAt = new \DateTime("now");
                
                $user_id = ($identity->sub != null) ? $identity->sub : null;
                $video_id = ($params->video_id != null) ? $params->video_id : null;
                $body = ($params->body != null) ? $params->body : null;
                $title = ($params->title != null) ? $params->title : null;
                
                if($user_id != null && $video_id != null) {
                    $em = $this->getDoctrine()->getManager();
                   
                    $user = $em->getRepository("BackendBundle:User")->find($user_id);
                    $vid = $em->getRepository("BackendBundle:Video")->find($video_id);
                    
                    $comment = new Comment();
                    $comment->setUser($user);//demande un objet, voir le setter
                    $comment->setVideo($vid);
                    $comment->setTitle($title);
                    $comment->setBody($body);
                    $comment->setCreatedAt($createdAt);
                    
                    $em->persist($comment);
                    $em->flush();
                    
                    $data = array("status"=>"success",
                                "code"=>200,
                                "msg"=>"Comment created",
                    );
                    
                }else {
                    $data = array("status"=>"error",
                                    "code"=>400,
                                    "msg"=>"Comment not created",
                        );
                }
            } else {
                $data = array("status"=>"error",
                        "code"=>400,
                        "msg"=>"Error, no json parameter",
                    );
            }
        } else {
            $data = array("status"=>"error",
                "code"=>400,
                "msg"=>"Not authorized to post comment",
                );
        }
        
        return $helpers->json($data);
    }
    
    public function deleteAction(Request $req, $comment_id = null) {
        $helpers = $this->get("app.helpers");
        
        $hash = $req->get("authorization", false);
        $authCheck = $helpers->authCheck($hash);
        
        if($authCheck == true) {
            $identity = $helpers->authCheck($hash, true);
            
            $user_id = ($identity->sub != null) ? $identity->sub : null;
            
            $em = $this->getDoctrine()->getManager();
            $comment = $em->getRepository("BackendBundle:Comment")->find($comment_id);
            
            if(is_object($comment) && $user_id != null){
                if(isset($identity->sub) && $identity->sub == $comment->getUser()->getId() || $identity->sub == $comment->getVideo()->getUser()->getId()) {
                    $em->remove($comment);
                    $em->flush();
                     $data = array("status"=>"success",
                                "code"=>200,
                                "msg"=>"Comment deleted",
                    );
                } else {
                    $data = array("status"=>"error",
                        "code"=>400,
                        "msg"=>"Thi is not your comment",
                        );
                }
            } else {
                $data = array("status"=>"error",
                    "code"=>400,
                    "msg"=>"Comment not deleted",
                    );
            }
        } else {
            $data = array("status"=>"error",
                "code"=>400,
                "msg"=>"Not authorized to delete comment",
                );
        }
        
        return $helpers->json($data);
    }
        
    public function listAction(Request $req, $video_id) {
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();
        
        $video = $em->getRepository("BackendBundle:Video")->find($video_id);
        $comments = $em->getRepository("BackendBundle:Comment")->findBy(array("video"=>$video), array('id'=>'desc'));
        
        if(count($comments)>=1) {
            $data = array("status"=>"success",
                "code"=>200,
                "data"=>$comments,
                );
        } else {
            $data = array("status"=>"error",
                "code"=>400,
                "msg"=>"No comment",
                );
        }
        return $helpers->json($data);
    }

}

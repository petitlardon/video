<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\User;
//use AppBundle\Services\Helpers;

class UserController extends Controller {
    public function newAction(Request $req) {
        $helpers = $this->get("app.helpers");
        
        $json = $req->get("json", null);
        $params = json_decode($json);
        $data = array("status"=>"error",
                        "code"=>400,
                        "msg"=>"This user already exists",
                    );
        
        if($json != null) {
            $createdAt = new \DateTime("now");
            $image = null;
            $role = "user";
            
            $email = (isset($params->email)) ? $params->email : null;
            $firstname = (isset($params->firstname) && ctype_alpha($params->firstname)) ? $params->firstname : null;
            $lastname = (isset($params->lastname) && ctype_alpha($params->lastname)) ? $params->lastname : null;
            $pass = (isset($params->password)) ? $params->password : null;
            
            $email_constraint = new Assert\Email();
            $email_constraint->message = "Email invalide";
            $validate_email = $this->get("validator")->validate($email, $email_constraint);
            
            if($email != null && count($validate_email) == 0 && $pass != null && $firstname != null && $lastname != null) {
                $user = new User();
                $user->setCreatedAt($createdAt);
                $user->setEmail($email);
                $user->setImage($image);
                $user->setRole($role);
                $user->setFirstname($firstname);
                $user->setLastname($lastname);
                
                $pwd = hash('sha256', $pass);
                $user->setPassword($pwd);
                
                $em = $this->getDoctrine()->getManager();
                $isset_user = $em->getRepository("BackendBundle:User")->findBy(array("email"=>$email));
                
                if(count($isset_user) == 0) {
                    $em->persist($user);
                    $em->flush();
                    
                    $data["status"] = "success";
                    $data["code"] = 200;
                    $data["msg"] = "New user created";
                } else {
                    $data = array("status"=>"error",
                        "code"=>400,
                        "msg"=>"This user already exists",
                    );
                }
            }
        }
        
        return new JsonResponse($data);
    }
    
    public function editAction(Request $req) {
        $helpers = $this->get("app.helpers");
        
        $hash = $req->get("authorization", NULL);
        $authCheck = $helpers->authCheck($hash);
        
        if($authCheck == true) {
            
            $identity = $helpers->authCheck($hash, true);
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository("BackendBundle:User")->findOneBy(array(
                "id"=>$identity->sub
            ));
            
            $json = $req->get("json", null);
            $params = json_decode($json);
            $data = array("status"=>"error",
                            "code"=>400,
                            "msg"=>"User not updated",
                        );

            if($json != null) {
                $createdAt = new \DateTime("now");
                $image = null;
                $role = "user";

                $email = (isset($params->email)) ? $params->email : null;
                $firstname = (isset($params->firstname) && ctype_alpha($params->firstname)) ? $params->firstname : null;
                $lastname = (isset($params->lastname) && ctype_alpha($params->lastname)) ? $params->lastname : null;
                $pass = (isset($params->password)) ? $params->password : null;

                $email_constraint = new Assert\Email();
                $email_constraint->message = "Email invalide";
                $validate_email = $this->get("validator")->validate($email, $email_constraint);

                if($email != null && count($validate_email) == 0 && $firstname != null && $lastname != null) {
                    $user->setCreatedAt($createdAt);
                    $user->setEmail($email);
                    $user->setImage($image);
                    $user->setRole($role);
                    $user->setFirstname($firstname);
                    $user->setLastname($lastname);

                    if($pass != null) {
                        $pwd = hash('sha256', $pass);
                        $user->setPassword($pwd);                        
                    }

                    $em = $this->getDoctrine()->getManager();
                    $isset_user = $em->getRepository("BackendBundle:User")->findBy(array("email"=>$email));

                    if(count($isset_user) == 0 || $identity->email == $email) {
                        $em->persist($user);
                        $em->flush();

                        $data["status"] = "success";
                        $data["code"] = 200;
                        $data["msg"] = "User updated";
                    } else {
                        $data = array("status"=>"error",
                            "code"=>400,
                            "msg"=>"User not updated",
                        );
                    }
                }
            }
        } else {
            $data = array("status"=>"error",
                            "code"=>400,
                            "msg"=>"Not authorized",
                );
        }
        
        return new JsonResponse($data);
    }
    
    public function uploadImageAction(Request $req) {
        $helpers = $this->get("app.helpers");
        
        $hash = $req->get("authorization", NULL);
        $authCheck = $helpers->authCheck($hash);
        
        if($authCheck == true) {
            $identity = $helpers->authCheck($hash, true);
            $em = $this->getDoctrine()->getManager();
            
            $user = $em->getRepository("BackendBundle:User")->findOneBy(array(
                "id"=>$identity->sub
            ));
            
            //upload
            $file = $req->files->get("image");
            
            if(!empty($file) && $file != null) {
                $ext = $file->guessExtension();
                if($ext == "jpeg" || $ext == "jpg" || $ext == "png" || $ext == "gif") {
                    $filename = time().".".$ext;
                    $file->move("uploads/users", $filename);

                    $user->setImage($filename);
                    $em->persist($user);
                    $em->flush();

                    $data = array("status"=>"success",
                                "code"=>200,
                                "msg"=>"Image uploaded",
                    );
                } else {
                    $data = array("status"=>"error",
                                "code"=>400,
                                "msg"=>"It's not an image",
                    );
                }
                
            } else {
                $data = array("status"=>"error",
                            "code"=>400,
                            "msg"=>"Image not uploaded",
                );
            }
        } else {
            $data = array("status"=>"error",
                            "code"=>400,
                            "msg"=>"Not authorized to access",
                );
        }
        
        return new JsonResponse($data);
    }
    
    public function channelAction(Request $req, $id_user = null) {
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository("BackendBundle:User")->find($id_user);
        $dql = "select v from BackendBundle:Video v where v.user = :id_user order by v.id desc";
            
        $query = $em->createQuery($dql)->setParameter("id_user", $id_user);
        
        $page =$req->query->getInt("page", 1);
        $paginator = $this->get("knp_paginator");
        $item = 10;
        
        $pagination = $paginator->paginate($query, $page, $item);
        $total_item = $pagination->getTotalItemCount();
        
        if(count($user) == 1) {
            $data = array("status"=>"success",
                "total_item"=>$total_item,
                "page"=>$page,
                "items_page"=>$item,
                "total_pages"=>ceil($total_item/$item)
                );
            $data["data"]["videos"] = $pagination;
            $data["data"]["user"] = $user;
            
        } else {
            $data = array("status"=>"error",
                "code"=>400,
                "msg"=>"User does not exists"
                );

        }
        return $helpers->json($data);
    }
}

<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
//use AppBundle\Services\Helpers;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }
    
    public function listAction(Request $request)
    {
        $helpers = $this->get("app.helpers");
        
        $hash = $request->get("authorization", null);
        $check = $helpers->authCheck($hash, true);

        return new JsonResponse($users);
    }
    
    public function loginAction(Request $req) {
        $helpers = $this->get("app.helpers");
        $jwt_auth = $this->get("app.jwt_auth");
        
        $json = $req->get('json', null);
        
        if($json != null) {
            $params = json_decode($json);
            $email = (isset($params->email)) ? $params->email : null;
            $pass = (isset($params->password)) ? $params->password : null;
            $hash = (isset($params->hash)) ? $params->hash : null;
            
            $email_constraint = new Assert\Email();
            $email_constraint->message = "Email invalide";
            
            $validate_email = $this->get("validator")->validate($email, $email_constraint);
            
            $pwd = hash('sha256', $pass);
            
            if(count($validate_email) == 0 && $pass != null) {
                if($hash == null) {
                    $signup = $jwt_auth->signup($email, $pwd);
                } else {
                    $signup = $jwt_auth->signup($email, $pwd, true);
                }
                return new JsonResponse($signup);
                //return $helpers->json($signup); // ne marche pas
            } else {
                return $helpers->json(array("status"=>"error", "data"=>"Login fail because of email or pass"));
            }
        } else {
            var_dump($json);die();
            return $helpers->json(array("status"=>"error", "data"=>"Login fail"));
        }
    }
    
}

create database if not exists video;
use video;

create table users(
    id int(255) auto_increment not null,
    role varchar(20),
    firstname  varchar(255),
    lastname varchar(255),
    email varchar(255),
    password varchar(255),
    image varchar(255),
    created_at datetime,
    constraint pk_users primary key(id)
) engine=InnoDb;

create table videos(
    id int(255) auto_increment not null,
    user_id int(255) not null,
    title  varchar(255),
    description text,
    status varchar(20),
    video_path varchar(255),
    image varchar(255),
    created_at datetime default null,
    updated_at datetime default null,
    constraint pk_videos primary key(id),
    constraint fk_videos_users foreign key(user_id) references users(id)
) engine=InnoDb;

create table comments(
    id int(255) auto_increment not null,
    user_id int(255) not null,
    video_id int(255) not null,
    title  varchar(255),    body text,
    created_at datetime default null,
    constraint pk_comment primary key(id),
    constraint fk_comment_video foreign key(video_id) references videos(id),
    constraint fk_comment_user foreign key(user_id) references users(id)
) engine=InnoDb;